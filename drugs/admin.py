from django.contrib.gis import admin
from models import Drug, Salt, Substitute

# Register your models here.
admin.site.register(Drug)
admin.site.register(Salt)
admin.site.register(Substitute)