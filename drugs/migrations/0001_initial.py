# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Drug'
        db.create_table(u'drugs_drug', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('pack', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('manufacturer_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('manufacturer_link', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('price', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'drugs', ['Drug'])

        # Adding model 'Salt'
        db.create_table(u'drugs_salt', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('drug', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['drugs.Drug'])),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('indicators', self.gf('django.db.models.fields.TextField')(default='')),
            ('typical_usage', self.gf('django.db.models.fields.TextField')(default='')),
            ('side_effect', self.gf('django.db.models.fields.TextField')(default='')),
            ('drug_interaction', self.gf('django.db.models.fields.TextField')(default='')),
            ('mechanism_of_action', self.gf('django.db.models.fields.TextField')(default='')),
            ('drug_link', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'drugs', ['Salt'])

        # Adding model 'Substitute'
        db.create_table(u'drugs_substitute', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('drug', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['drugs.Drug'])),
            ('drug_name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('drug_link', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('pack', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('manufacturer_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('price', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'drugs', ['Substitute'])


    def backwards(self, orm):
        # Deleting model 'Drug'
        db.delete_table(u'drugs_drug')

        # Deleting model 'Salt'
        db.delete_table(u'drugs_salt')

        # Deleting model 'Substitute'
        db.delete_table(u'drugs_substitute')


    models = {
        u'drugs.drug': {
            'Meta': {'object_name': 'Drug'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manufacturer_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'manufacturer_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'pack': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'drugs.salt': {
            'Meta': {'object_name': 'Salt'},
            'drug': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['drugs.Drug']"}),
            'drug_interaction': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'drug_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'indicators': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'mechanism_of_action': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'side_effect': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'typical_usage': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'drugs.substitute': {
            'Meta': {'object_name': 'Substitute'},
            'drug': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['drugs.Drug']"}),
            'drug_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'drug_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manufacturer_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'pack': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        }
    }

    complete_apps = ['drugs']