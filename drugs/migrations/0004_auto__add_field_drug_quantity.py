# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Drug.quantity'
        db.add_column(u'drugs_drug', 'quantity',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Drug.quantity'
        db.delete_column(u'drugs_drug', 'quantity')


    models = {
        u'drugs.drug': {
            'Meta': {'object_name': 'Drug'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manufacturer_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'manufacturer_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'pack': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'drugs.salt': {
            'Meta': {'object_name': 'Salt'},
            'drug': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['drugs.Drug']"}),
            'drug_interaction': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'drug_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'indicators': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'mechanism_of_action': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'side_effect': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'typical_usage': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'drugs.substitute': {
            'Meta': {'object_name': 'Substitute'},
            'drug': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['drugs.Drug']"}),
            'drug_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'drug_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manufacturer_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'pack': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'price': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        }
    }

    complete_apps = ['drugs']