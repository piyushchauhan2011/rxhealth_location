from django.contrib.gis.db import models

# Create your models here.
class Drug(models.Model):
	name = models.CharField(max_length=255,default="")
	pack = models.CharField(max_length=255,default="")
	manufacturer_name = models.CharField(max_length=255,null=True)
	manufacturer_link = models.CharField(max_length=255,null=True)
	quantity = models.IntegerField(default=1)
	drug_match_link = models.CharField(max_length=255,default="")
	price = models.CharField(max_length=255,null=True)
	url = models.CharField(max_length=255,null=True)

	def __unicode__(self):
		return self.name

class Salt(models.Model):
	drug = models.ForeignKey(Drug)
	name = models.CharField(max_length=255,default="")
	link = models.CharField(max_length=255,null=True)
	indicators = models.TextField(default="")
	typical_usage = models.TextField(default="")
	side_effect = models.TextField(default="")
	drug_interaction = models.TextField(default="")
	mechanism_of_action = models.TextField(default="")

	def __unicode__(self):
		return self.name

class Substitute(models.Model):
	drug = models.ForeignKey(Drug)
	drug_name = models.CharField(max_length=255,default="")
	drug_link = models.CharField(max_length=255,null=True)
	pack = models.CharField(max_length=255,null=True)
	manufacturer_name = models.CharField(max_length=255,null=True)
	price = models.CharField(max_length=255,null=True)

	def __unicode__(self):
		return self.drug_name