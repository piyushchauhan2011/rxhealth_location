from models import Drug,Salt,Substitute

import requests,lxml
from lxml import html

def run(verbose=True):
	print 'Started Scraping healthkartplus.com drug database'
	for i in range(4842,75882):
		# try:
		r = requests.get('http://www.healthkartplus.com/details/drugs/'+str(i)+'/op-650-mg')
		root = lxml.html.fromstring(r.content)

		drug = Drug()
		drug.url = r.url
		print 'Scraping: ' + r.url
		# drug name
		tmp = root.cssselect('div.item .item-name')
		if(tmp):
			drug.name = tmp[0].text_content().strip()
		# drug pack
		tmp = root.cssselect('div.item .item-pack')
		if(tmp):
			drug.pack = tmp[0].text_content().strip()
		# drug manufacturer name
		tmp = root.cssselect('div.item .item-manufacturer a')
		if(tmp):
			drug.manufacturer_name = tmp[0].text_content().strip()
		# drug manufacturer link
		tmp = root.cssselect('div.item .item-manufacturer a')
		if(tmp):
			drug.manufacturer_link = 'http://www.healthkartplus.com' + tmp[0].get('href')
		# drug quantity
		tmp = root.cssselect('div.item .item-qty-value')
		if(tmp):
			drug.quantity = int(tmp[0].text_content().strip())
		# drug price
		tmp = root.cssselect('div.item .item-price')
		if(tmp):
			drug.price = tmp[0].text_content().strip()
		# drug match link
		try:
			drug.drug_match_link = 'http://www.healthkartplus.com' + root.cssselect('.col-sm-5')[1].cssselect('a')[0].get('href')
		except IndexError:
			print 'Drug Match link Not Found'
		drug.save()

		salt = Salt()
		salt.drug = drug
		# salt name and link
		tmp = root.cssselect('a.salt-name')
		if(tmp):
			salt.name = tmp[0].text_content().strip()
			salt.link = 'http://www.healthkartplus.com' + tmp[0].get('href')
		# salt indicators
		try:
			salt.indicators = root.cssselect('.col-sm-6')[1].cssselect('span')[0].text_content().strip()
		except IndexError:
			print 'Salt Indicators Not Found'
		# salt typical usage
		try:
			salt.typical_usage = root.cssselect('.col-sm-6')[1].cssselect('p')[0].text_content().strip()
		except IndexError:
			print 'Salt Typical Usage Not Found'
		# salt side effect
		try:
			salt.side_effect = root.cssselect('.col-sm-6')[1].cssselect('p')[1].text_content().strip()
		except IndexError:
			print 'Salt SideEffect Not Found'
		# salt drug interaction
		try:
			salt.drug_interaction = root.cssselect('.col-sm-6')[1].cssselect('p')[2].text_content().strip()
		except IndexError:
			print 'Salt Drug Interaction Not Found'
		# salt mechanism of action
		try:
			salt.mechanism_of_action = root.cssselect('.col-sm-6')[1].cssselect('p')[3].text_content().strip()
		except IndexError:
			print 'Salt mechanism of action Not Found'
		salt.save()

		tmp = root.cssselect('ul.gl.item-list li')
		for t in tmp:
			substitute = Substitute()
			substitute.drug = drug
			# substitute drug name and link
			try:
				substitute.drug_name = t[0].cssselect('.item-name')[0].text_content().strip()
				substitute.drug_link = 'http://www.healthkartplus.com' + t[0].cssselect('.item-name')[0].get('href')
			except IndexError:
				print 'Substitute Drug Name or Link Not Found'
			# substitute pack
			try:
				substitute.pack = t[0].cssselect('.item-pack')[0].text_content().strip()
			except IndexError:
				print 'Substitute Pack Not Found'
			# substitute manufacturer
			try:
				substitute.manufacturer_name = t[0].cssselect('.item-manufacturer')[0].text_content().strip()
			except IndexError:
				print 'Substitute manufacturer Not Found'
			# substitute price
			try:
				substitute.price = t[0].cssselect('.item-price')[0].text_content().strip()
			except IndexError:
				print 'Substitute price Not Found'
			substitute.save()
		print 'Saved Data Url: ' + r.url
		# except:
		# 	print 'Some Error Occurred!'
	print 'Scraping Finished'