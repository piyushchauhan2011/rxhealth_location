from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rxhealth_location.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^hospitals_latlng/', 'hospitals.views.hospitals_latlng'),

    url(r'^admin/', include(admin.site.urls)),
)
