--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: hospitals_hospital; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY hospitals_hospital (id, name, address, phone, point) FROM stdin;
121	Ram Janki Memorial Vijayvergia Hospital	9 Vishnupuri Jagatpura Road, Jaipur, Rajasthan - 302006	1412751100	0101000020E61000007A85AA3D11F55240A704C4245CD83A40
56	Rainbow Hospital	Sanganer Tower, Near By Idgah Masjid, Deegi Road, Sanganer, Jaipur, Rajasthan - 302030	0	\N
2	Cocoon Hospital	Plot No – 14, Airport Plaza, Durgapura, Tonk Road, Jaipur, Rajasthan - 302016	1415169699	0101000020E6100000306C281051F2524057282DB70FEA3A40
3	Dr. Singhs Homeopathy	shop no-2, 260 sayar hights, Vivek vihar, New sanganer road, sodala, Jaipur, Rajasthan - 302019	9672133330	0101000020E6100000CBDF185D39F15240E1DF5630E0E33A40
5	Anita Children Hospital	189, Heera Nagar Dcm, Ajme Road, Jaipur, Rajasthan - 341001	332245795	0101000020E61000003E01B969D86C5240A95780A5A9483B40
6	Jaipur Pain Relief Centre	Gurudwara, Main Govind Marg, Near Transport Nagar, Sethi Colony, Jaipur, Rajasthan - 302004	1412600991	0101000020E6100000FA35EDBDD3F5524047CB811E6AE73A40
7	Anshu Hospital	B-209, Rajendra Marg, Bapu Nagar, Jaipur, Rajasthan - 302015	1412710801	0101000020E61000002FD978B0C5F35240786E90FFB8E33A40
9	Joyatiba Hospital	Chandpole, Jalupura, Sansar Chand Road , Jaipur, Rajasthan - 302001	1412376902	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
10	Om Mother & Child Hospital	100 E, Pravashi Nagar, Murlipura, Jaipur, Rajasthan - 302039	9660382448	0101000020E6100000C3D4963AC8F152404A18175811F53A40
11	Kidney Care And General Hospital	315, Sri Gopal Nagar, Gopalpura Bypass, Jaipur, Rajasthan - 302019	1412504257	0101000020E6100000D3E01170E3F052402437D43950E13A40
12	Badrinath Health Clinic	Golya Talkies, Ajmeri Gate, Jaipur, Rajasthan - 302001	9320013200	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
13	Dr. Krishnas Cancer Healer Center	49, Cosmo Colony, Amprapali Circle, Near Easy Day, Vaishali Nagar, Jaipur, Rajasthan - 302012	0	0101000020E610000080A9B063EDEF524031BC48579FE73A40
14	Rajasthan Nursing Home & Eye Centre	Gopalapur Bypass, Jaipur, Rajasthan - 302015	1412503015	0101000020E61000005FEF593222F2524061A4BCFB88DE3A40
16	Rehman Clinic	Link Road, M I Road, Bapu Bazar, Ajmeri Gate , Jaipur, Rajasthan - 302003	9024494327	0101000020E61000000AA58A3D0FF652404746BD9646EA3A40
17	R.K. Yadav Memorial Mental Health and Deaddiction Hospital	Vidyanagar, B-6, Hanuman Nagar, Sirsi Road, Jaipur, Rajasthan - 302021	1412350492	0101000020E6100000C50E74FE37EF5240E1849DBD8EEB3A40
18	Metro Hospital	Jaswant Nagar, 203, Officers Campus Colony, Sirsi Road, Jaipur, Rajasthan - 302012	1412355948	0101000020E6100000CACCBBA074EF52407A275591C0EB3A40
19	Marudhar Hospital	A-93-99, Singh Bhoomi, Khatipura, Jaipur, Rajasthan - 302001	1412357570	0101000020E610000038E5C061D8EF52400C0C0F50D0EC3A40
20	Jain Nursing Home	Control Room, Ajmeri Gate, Jaipur, Rajasthan - 302001	9821421210	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
22	Ashoka Hospital	48, Prawasi Nagar, Murlipura, Jaipur, Rajasthan - 302020	1412230399	0101000020E6100000AD8383BD09F15240D93D1E9F35DA3A40
24	Dave Ayurvedic Clinic	2/196, Agarwal Farm, Mansarovar, Jaipur, Rajasthan - 302020	1412395438	0101000020E610000061F8889892F15240B7BD384C8FD73A40
25	Dwarka Orthopaedics & Trauma Hsptal	Shyam Nagar, Ajmer Road, Jaipur, Rajasthan - 302006	0	0101000020E6100000BFD8D6AA38F15240D010E91269E63A40
26	Gopinath Hospital	Phuta Khurra, Ramganj Bazaar, Ramganj, Jaipur, Rajasthan - 302003	0	0101000020E61000006C19BA232DF5524049B31E6393EB3A40
27	Dr. Roy Chowdhurys Institute Of Urology And Laparoscopic Surgery	A - 4 - B, Tilak Marg, C Scheme, Jaipur, Rajasthan - 302001	1412226129	0101000020E6100000388E0EFE34F3524081994A9A9AE73A40
28	Kerala Ayurveda	52, Dhuleshwar Bagh, C - Scheme, Sardar Patel Marg, Jaipur, Rajasthan - 302001	1414022422	0101000020E610000090FA46CF08F352405E6743FE99E93A40
29	Prakash Nethralaya & Panchakarma Kendra	4/18, Surya Path, Jawahar Nagar, Jaipur, Rajasthan - 302004	1412650121	0101000020E61000001E914A0CDDF45240FBE02131E6E33A40
30	Raksheet Hospital	94/192, Vijay Path, Madhyam Marg, Mansarovar, Jaipur, Rajasthan - 302020	0	0101000020E6100000278B56493FF15240B60D486705D93A40
32	J.P.Eye Hospital	B - 14, LAL Kothi Shopping Centre, Tonk Road, Jaipur GPO, Jaipur, Rajasthan - 302001	0	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
33	Rungta Children & General Hospital	73/4a, Magnika, Near Kv No. 5, Near Police Thana, Jhalana, Kaligiri Road, Malviya Nagar, Jaipur, Rajasthan - 302017	1414039999	0101000020E6100000BCD3539A03F45240C889C0A26CDB3A40
34	Amar Medical & Research Centre	Sector - 3, Kiran Path, Jaipur Gpo, Jaipur, Rajasthan - 302001	1412391892	0101000020E6100000A5F908A1A8F05240D7FB8D76DCDC3A40
35	Ram Kutiya Hospital	71, Awadpuri, Kalwar Road, Jaipur GPO, Jaipur, Rajasthan - 302001	0	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
36	Vijay Hospital	B -139, Vijay Path, Tilak Nagar, Jaipur, Rajasthan - 302004	1412622283	0101000020E61000003931DA9978F45240BDE5EAC726E53A40
38	Lalkothi Hospital	A - 43, Krishna Nagar - 2, Janpath, LAL Kothi, Jaipur, Rajasthan - 302015	1412741689	0101000020E6100000363F598C15F35240B3BC06D8A2E43A40
39	Dr. Mamodias Proctology & IRC Centre	264, Gurunanak Pura, Raja Park, Jaipur, Rajasthan - 302004	1412624077	0101000020E6100000F63C2421B6F45240265BB8077CE53A40
41	Chakrapani Ayurveda Clinic & Research Center	8 Diamond Hill, Behind Birla Temple, Tulsi circle, Shanti path, Jaipur, Rajasthan - 302004	1412624003	0101000020E6100000BD5BB45B26F55240588CBAD6DEE33A40
42	Kalla Gastro Hospital	78, Dhuleshwar Garden, Jaipur Gpo, Jaipur, Rajasthan - 302001	9829024065	0101000020E6100000F2EA1C0332F35240A592F07332E73A40
43	Satyam Hospital	Phulera, Jaipur, Rajasthan - 303338	9351553023	0101000020E6100000098A1F63EECE524076711B0DE0DD3A40
45	B B Saxena Memorial Hospital	114, Khatipura, Gom Defence Colony, Gautam Marg, Sirsi Road, Valishali Nagar, Jaipur, Rajasthan - 302021	1412358396	0101000020E6100000B0743E3C4BF05240DF707A72A8E83A40
46	Arya Hospital	24, Near J K Loan Hospital, Jln Marg, Chetak Marg, Jaipur, Rajasthan - 302015	1412612160	0101000020E61000003090B1248AEE5240F79D150786E03A40
47	Siddhi Vinayak Hospital	E - 14, Anand Vihar Colony, Jagatpura Road, Jhotwara, Jaipur, Rajasthan - 302016	0	0101000020E6100000BBC7E3B386F25240276728EE78EF3A40
48	Choudhary Hospital	Bhagasara School KE Pas, Chirnotiya Road, Jobner, Jaipur, Rajasthan - 303328	9782946897	0101000020E61000006066E95443D85240648B492476F83A40
49	Sukh Sagar Hospital	10, Shopping Centre, Ambabari, Jaipur, Rajasthan - 302012	0	0101000020E6100000D4EE5701BEF1524068CD8FBFB4F03A40
50	Sanjeevani Hospital & Medical Research Institute	Laxman Path, 1, Sodala, New Sanganer Road, Shyamnagar, Jaipur, Rajasthan - 302019	0	0101000020E6100000CBDF185D39F15240E1DF5630E0E33A40
51	Child Care Hospital	B - 76, Anandpuri, M D Road, Jaipur, Rajasthan - 302004	1412610080	0101000020E6100000914CE207C2F45240DE3EABCC94E43A40
53	Mahesh Hospital	Near Chand Pole Gate, Top Khana Ka Rasta, Chandpole Bazar, Jaipur, Rajasthan - 302001	1412320810	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
54	Ishwar Memorial Hospital	Madhyam Marg, 55/4, Opposite Heera Path Post Office, Mansarovar, Jaipur, Rajasthan - 302020	9983838880	0101000020E610000000C22C593AF1524045F9DDCF84D93A40
55	Jha Hospital & Research	Modi Markett, Near Panchyat Samiti, Kalwar Road, Jhotwara, Jaipur, Rajasthan - 302012	1412345500	0101000020E61000004322C89C42EF5240DEBC827EF0F13A40
126	Kamdar Children Hospital	5, Opp Sodala Police Station, Vivek Market, Ajmer Road, Sodala, Jaipur, Rajasthan - 302006	1412224961	0101000020E61000000AE0C1AA55F15240D4FF8359B2E63A40
65	Shaifali Hospital	259, Sec - 2, Near Petrol Pump, Vidhyadhar Nagar, Jaipur, Rajasthan - 302023	9413157001	\N
75	Ganadhipati Purushottam Shekhawati Hospital & Research centre	A/2,Opp Time Square, Central spine, Vidhyadhar Nagar, Jaipur, Rajasthan - 302023	1412232211	\N
79	Shriyank Hospital & Research Center	Sanganer Tower, Near By Idgah Masjid Deegi Road, Sanganer, Jaipur, Rajasthan - 302030	1412392390	\N
58	K.K.Hospital	Khawaj J1 KA Rasta, Hawamahal, Jaipur GPO, Jaipur, Rajasthan - 302001	1412410121	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
59	Honey Hospital	B - 10, Vidhyut Nagar - B, Ajmer Road, Jaipur, Rajasthan - 302019	0	0101000020E6100000E8A3E77173F052405C4C7D6A06E63A40
60	Kalptaru Hospital	94/7, Kumbha MG, Pratapnagar, Jaipur, Rajasthan - 302030	0	0101000020E6100000B65C47663FF4524073486AA164CC3A40
62	Dr. Virendra Laser, Phaco Surgery Clinic	Shyam Anukampa, Opp HDFC, Ashok Marg, Ahinsa Circle, C - Scheme, Jaipur, Rajasthan - 302001	1412373725	0101000020E6100000573C9A8F90F35240C57AFE0F0BEA3A40
63	Child Care Hospital	M.D. Road, Jaipur, Rajasthan - 302004	0	0101000020E61000004552C197F8F15240136D7D4743F63A40
64	Smile Carve Dental Clinic	Nandan Apartments, C-72 Sarojini Marg, C-Scheme, Jaipur, Rajasthan - 302001	8696033160	0101000020E61000008F8D40BCAEF35240F63D8FF6E4E83A40
67	Amogh Hospital	Ram Gali No. 6, Raja Park, Jaipur, Rajasthan - 302004	1412612187	0101000020E6100000913E52E94CF552401D4762388DE53A40
68	Neha Maternity & Ent Centre	72, Devi Nagar, Shyamnagar, Jaipur, Rajasthan - 302019	0	0101000020E610000008F13790E4F05240C3C8DCC6B0E23A40
69	Preeti Maternity & Genral Hospital	56/122, Rajat Path, Mansarovar, Jaipur, Rajasthan - 302020	1412785264	0101000020E6100000E53B9DD090F0524022D5C10B6CDD3A40
71	Curewell Hospital	1/51 - 53,SFS, Agarwal Farm, Mansarovar, Jaipur, Rajasthan - 302020	1412396379	0101000020E610000061F8889892F15240B7BD384C8FD73A40
72	Apaji Arogya Mandir	Bansthali Vidhyapit, Jaipur, Rajasthan - 304022	1412284640	0101000020E61000000F05D8EC6DF35240F85278D0ECE83A40
73	Jeevan Shree Hospital & Fertility Centre	5, Gulbari, Near Sahid Mangej Singh Petrol Pump, Jhotwara, Jaipur, Rajasthan - 302016	1412341698	0101000020E6100000BBC7E3B386F25240276728EE78EF3A40
74	Manochikitsa & Nasha Mukti Kendra	B - 6, Hanuman Nagar, Sirsi Road, Jaipur, Rajasthan - 302021	1412357584	0101000020E6100000C50E74FE37EF5240E1849DBD8EEB3A40
77	Ratan Netralaya	Vijay Path, Tilak Nagar, Jaipur, Rajasthan - 302004	9828072585	0101000020E61000003931DA9978F45240BDE5EAC726E53A40
78	City Hospital	93, Doctors Colony, Near DCM, Amer Road, Jaipur, Rajasthan - 302006	0	0101000020E61000002EE57CB177F65240CD76853E58F83A40
80	Uniara Hospital & Research Centre	20, Uniara Garden, Near Ganesh Mandir, Moti Doongri Road, Pushpa Path, Jaipur, Rajasthan - 302004	1412622907	0101000020E6100000914CE207C2F45240DE3EABCC94E43A40
81	Chitrakoot Hospital	Janki Marg, 10/46, Opposite Chitrakoot Stadium, Vaishali Nagar, Jaipur, Rajasthan - 302021	9414796915	0101000020E610000080A9B063EDEF524031BC48579FE73A40
82	Dr. M R Grewal Memorial Hospital	A - 22 B, Near Pital Factoy, Shastri Nagar, Jaipur, Rajasthan - 302016	9829066516	0101000020E61000005FBAA4C574F35240543882548AF33A40
83	Sahai Hospital	Bhabha Marg Moti Dungri, Jaipur, Rajasthan - 302004	1412621888	0101000020E6100000826D69DA6AF45240614557337CE53A40
85	Getwell Poly Clinic & Hospital	Jawahar Lal Nehru Marg, Jaipur, Rajasthan - 302001	1412563743	0101000020E6100000147F6F2ED9F3524093DC065A26E03A40
86	Agrasen Orthopaedic Hospital	C-6, Malviya Nagar, Jaipur, Rajasthan - 302017	1412522781	0101000020E6100000F8E17BDABCF45240600FDC37ADD63A40
87	Mahila Chikitsalaya	Sanganeri Gate, Jaipur, Rajasthan - 302003	1412601333	0101000020E61000004B6D4ECAC9F45240056FA35C75EA3A40
88	Jaipur Calgary Eye Hospital	Malvia Nagar, Jaipur, Rajasthan - 302017	1412521384	0101000020E6100000BCD3539A03F45240C889C0A26CDB3A40
90	Sparsh Hospital	New Sanganer Road, Sanganer, Jaipur, Rajasthan - 302030	1414362698	0101000020E61000007374A61A87F15240ECAA9BE678D43A40
91	Nims Hospital	Delhi National Highway, Shobha Nagar, Jaipur, Rajasthan - 303001	0	0101000020E61000007E912B50E60C5340E090FC7733133B40
93	Ehcc Eternal Heart Care Centre	Near Jawahar Circle, 3 A Jagatpura Road, Jaipur, Rajasthan - 302020	7665022224	0101000020E6100000AD8383BD09F15240D93D1E9F35DA3A40
94	Katta Hospital & orthopaedic Center	47-52, Amba Bari, Jaipur, Rajasthan - 302023	1412336097	0101000020E61000004E29AF95D0F15240A76D0DB622F13A40
95	Poddar Clinic and Diagnostic Centre	3510AB, Langar Ke Balaji Ka Rasta, Gangori Bazar, Jaipur, Rajasthan - 302001	1412411655	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
96	Rajasthan Wellness Clinic	B-22, Geetanjali Tower, Ajmer Road, Sodala, Jaipur, Rajasthan - 302006	7877647766	0101000020E6100000C34252B0EBF15240D49AE61DA7E83A40
97	Dr. Badayas Speciality Homoeopathic Clinic	B-6-7, Dwarika Tower, Central Spine, Vidhyadhar Nagar, Jaipur, Rajasthan - 302023	1412336466	0101000020E610000087CBE0DEEBF152407AC95A9E62F63A40
99	D2S Ayurveda Clinic	B-50, Naya Ghar, Shanti Nagar, Gujar Ki Thadi, Shyamnagar, Jaipur, Rajasthan - 302019	9414321055	0101000020E6100000CBDF185D39F15240E1DF5630E0E33A40
100	Shri Vinayak Uro Gynaec Clinic	Panchsheel Enclave, 26 A, Pani ki tanki wali gali, Durgapura, Jaipur, Rajasthan - 302018	9828708222	0101000020E6100000CBD6FA2221F2524014D09F9163D83A40
101	Abhishek Hospital	3-B-iii, Near Dharam Singh Circle, Moti Doongri Road, Jaipur, Rajasthan - 302004	9829013567	0101000020E610000059405711A4F45240E014B1E31AE83A40
102	Jaipur Heart Institute	Lal Kothi, Near S.M.S. Stadium, Tonk Road, Jaipur, Rajasthan - 302015	1412742266	0101000020E6100000DD3DE53D73F35240BAF94674CFE43A40
103	Searoc Cancer Center	S.K. Soni Hospital Premise, Sikar Road, Sector-5 Vidhyadhay Nagar, Jaipur, Rajasthan - 302013	1412233337	0101000020E61000001DDC42FC68F15240305118EF22053B40
105	Maharshi Charak Ayurveda Clinic & Research Center	Saket E - 7, Kanti Chandra Road, Banipark, Jaipur, Rajasthan - 302016	1412205628	0101000020E6100000BBC7E3B386F25240276728EE78EF3A40
106	Dr. Virendra Laser, Phaco Surgery Centre	Tonk Phatak, Behind Mahindra Showroom, Tonk Road, Gandhi Nagar, Jaipur, Rajasthan - 302015	1412707580	0101000020E61000000955C5AF33F35240A07719B446E13A40
107	Gangori Hospital	Gangori Bazaar, Jaipur, Rajasthan - 302004	1412329050	0101000020E6100000727CFEC57DF4524007E0B07E7DEE3A40
108	Dr.K.C. Kasliwals ENT Centre	Panch Batti, Mani Mahal Road, Jaipur, Rajasthan - 302001	1412360331	0101000020E6100000788489F5FCF45240DB6CACC43CED3A40
110	Tarani Hospital	122/218 - 219, Agarwal Farm, 122/69, Mansarovar, Jaipur, Rajasthan - 302020	1412781256	0101000020E610000061F8889892F15240B7BD384C8FD73A40
111	RKV Maxima Heart Institute & Reaserch Center	D - 78, Bani Park, Ghiya Marg, Jaipur, Rajasthan - 302006	1412281225	0101000020E6100000C3D4963AC8F15240F595517644E93A40
112	Shri Hospital	4, Vishnupuri, Opp Deepal Restaurant, Jagatpura Road, Jagat Pura, Jaipur, Rajasthan - 302017	1412752880	0101000020E6100000C9E53FA4DFF552409249FC40A8DD3A40
113	Swasthya Kalyan Hospital	5449, Prem Bhawan, Kgb Ka Rasta, 4th Crossing, Johri Bazar, Jaipur, Rajasthan - 302001	1412572215	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
118	Mohammed Hospital	Bh Gota Factory, Galta Road, Jaipur, Rajasthan - 302003	1412605843	0101000020E61000006884C83379F552402FBFD364C6EB3A40
119	Gulati Dental Centre	1596, Shakhwati Bldg, Shop No. 357, Ghat Gate, Jaipur, Rajasthan - 302003	1412566653	0101000020E61000000AA58A3D0FF652404746BD9646EA3A40
120	Advanced Neurology & Superspeciality Hospital	D - 358, Brain Tower, Malviya Nagar, Jaipur, Rajasthan - 302015	1412724258	0101000020E61000004DCA935ADEF35240D9BB9A971EDA3A40
122	Vijay Vergia Hospital	9, Vishnupuri, Main Road, Jagat Pura, Jaipur, Rajasthan - 302017	1412751100	0101000020E61000007A85AA3D11F55240A704C4245CD83A40
115	Sunni Hospital	3818, Inder LAL Ji Ka Rasta, Chandpole Bazar, Jaipur, Rajasthan - 302001	1412314358	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
116	Durga Poly Clinic	Near Kama House, Ajmer Road, Sodala, Jaipur, Rajasthan - 302006	1412450837	0101000020E6100000A199CC1DA2F152401374C5D67DE73A40
123	Purohit Eye Hospital	8/C - 8, Tonk Phatak, Pratap Nagar, Jaipur, Rajasthan - 302030	1412593430	0101000020E6100000B1B90FE5B0F2524080FBF0E2B3E13A40
124	Rawal Hospital	Bhankrota, Ajmer Road, Jaipur, Rajasthan - 303011	1412250084	0101000020E610000063B0879B78EC52404FFD288F13DF3A40
125	Kalia Hospital & Reserch Center	122/144, Agrwal Farm, Vijay Path, Mansarovar, Jaipur, Rajasthan - 302020	1412781527	0101000020E6100000A53CA9E53DF152408705AD7603D93A40
127	Narmada Hospital And Diagnostic Centre	E-466, Sikar Road, Jaipur, Rajasthan - 302012	1412260399	0101000020E61000003F3C4B9091F15240E635C01605FB3A40
128	Ghiya Hospital	E - 68, Siddharth Nagar, Sector-12, Malviya Nagar, Jaipur, Rajasthan - 302017	1412547279	0101000020E6100000BE2C921BEAF3524048307A13E8D83A40
129	Raj Khurana Memorial Hospital	B - 14/D, Adarsh Nagar, Near Jamna Tower, Govind Marg, Tilak Nagar, Jaipur, Rajasthan - 302004	1414024041	0101000020E6100000AA0A57514DF45240F2BEE0890FE63A40
130	Ratnu Ent Hospital	1/412, Sec - 1, Vidhyadhar Nagar, Jaipur, Rajasthan - 302023	1412236162	0101000020E6100000EB72A5F9BEF152409014916115F73A40
132	Pawan Hospital	Govind Puri (E), Amer Road, New Ramgarh MOD, Jaipur, Rajasthan - 302002	1412630954	0101000020E61000007D073F7180F552400BA9EC05B0F03A40
136	Centre for Sight, Safdarjung Enclave	 B - 5/24, Safdarjung Enclave, Opp. Deer Park, Delhi, Delhi - 110029 	1145738888	0101000020E6100000324EE89A6E4C53408570164042903C40
137	Fortis Escorts Hospital	 Jawahar Lal Nehru Marg, Malvia Nagar, Jaipur, Rajasthan - 302107    	1412547000	0101000020E61000003E1581FB4BF35240E25CC30C8DD73A40
138	S.K. Soni Hospital	 Vidhyadhar Nagar, Sector 5, Main Sikar Road, Jaipur      	1415164000	0101000020E61000003F3C4B9091F15240E635C01605FB3A40
139	Santokba Durlabhji Memorial Hospital	 Bhawani Singh Marg, Jaipur 	1412566251	0101000020E6100000E1229CBB82F352409142FE3E88E53A40
141	Soni Hospital	 38, Kanota Bagh, Jawahar Lala Nehru Marg, Jaipur 	4145163700	0101000020E6100000B234A61600F05240B299E89898E73A40
142	Apex Hospitals	 Apex Circle, Malvia Nagar, Jaipur 	1412751871	0101000020E6100000C28366D7BDF45240D05DB7ADD2DA3A40
143	S M S Hospital	 Near Ajmeri Gate, Jawahar Lal Nehru Marg, Jaipur 	1412560291	0101000020E6100000B234A61600F05240B299E89898E73A40
144	Dhanwantari Hospital & Research Centre	 Mansarovor, New Sanganer Road, Jaipur 	1412781425	0101000020E61000008A89720EC3F05240C6E6994D36DA3A40
146	Bhagwan Mahaveer Cancer Hospital and Research Centre	 Jawahar Lal Nehru Marg, Jaipur 	1412700107	0101000020E6100000ABA6351749F35240E03CE64D8FD73A40
147	Rungta Hospital	 Calgiri Road, Malviya Nagar,Jaipur 	1414039999	0101000020E6100000BCD3539A03F45240C889C0A26CDB3A40
148	Anand Hospital & Eye Centre	 21, Bharat Mata Lane, Jamna Lal Bajaj Marg, C-Scheme, Jaipur 	1412371107	0101000020E61000007CDDD8A2BBF252401A1A4F0471E83A40
149	Jaipur Hospital	 Mahavir Nagar, Gopalapura Flyover, Sitapura, Tonk Road, Jaipur 	1412551500	0101000020E61000001EF7521D97F55240EF1A99EC44C43A40
151	ASG Eye Hospitals	 D - 247, Bihari Marg, Bani Park, Jaipur 	1412200581	0101000020E61000007AF2FADD85F252407C1F589FBCED3A40
152	Marudhar Hospital	 A 93 - 99, Singh Bhoomi, Khatipura Road, Jaipur 	1412356944	0101000020E610000038E5C061D8EF52400C0C0F50D0EC3A40
154	Dr. Agarwals Eye Hospital	 A - 2, Jamnalal Bajaj Marg, Civil Lines Railway Crossing, C - Scheme, Jaipur 	1413980200	0101000020E610000000B49C3A45F25240481B47ACC5EB3A40
155	Imperial Hospital	 Near Main Circle, Shastri Nagar, Jaipur 	1412300111	0101000020E6100000D5004AE8D3F2524039AAE4AD04F43A40
156	Tongia Heart And General Hospital	 7, Vivekananda Marg, C Scheme, Jaipur 	1412370271	0101000020E61000005FE2D92FE9F35240CE6F986890E83A40
157	Jaipur Dental Hospital & Orthodontic Centre	 9, Gopinath Marg, New Colony, Near Panch Batti,M.I. Road, Jaipur 	1412368183	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
158	K C Memorial Eye Hospital	 Malan Ka Chauraha, Malviya Marg, C - Scheme, Jaipur 	1412372642	0101000020E610000093AF5F0B55F35240EFA600BD15EA3A40
160	Shubh Hospital	 A-35, Vidhyut Nagar Crossing, Main Ajmer Road, Jaipur 	1412357223	0101000020E6100000684F13C74BF15240C1069F419EE63A40
161	Manu Hospital & Research Centre	 Sodala, Shyamnagar, Jaipur 	1412292530	0101000020E61000008E194DD30CF152400A359E639BE53A40
162	Baheti Hospital	 14, Usha Colony, Malviya Nagar Main Road, Jaipur 	1412754049	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
164	Khandaka Hospital	 160 - 161, Opp Sanghi Farm House, Kailash Puri, Tonk Road, Durgapura, Jaipur 	1412548211	0101000020E61000005D990178D5F252402A4A534EC5D93A40
165	Malpani Multispecialty Hospital	 S.P. 6, Road No. 1 V.K.I. Area, Sikar Road, Jaipur 	1412261315	0101000020E61000007323D1706FF1524060E7A6CD38F93A40
166	Railway Hospital, Jaipur	 Station Road, Sindhi Camp, Jaipur 	1412223735	0101000020E610000082C5E1CC2FF3524005FD851E31EC3A40
167	Knee and Shoulder Clinic	 B-78, Triveni Nagar, Tonk Road, Jaipur 	1412761897	0101000020E6100000BC5CC477E2F25240B534B74258DB3A40
169	Mahatma Gandhi Medical Hospitals	 RIICO Institutional Area, Tonk Road, Sitapura, Jaipur 	1412770798	0101000020E61000001EF7521D97F55240EF1A99EC44C43A40
170	Bhandari Hospital & Research Centre	 138-A, Vasundhra Colony, Tonk Road, Gopalpura  Bypass, Jaipur 	1412703851	0101000020E6100000AE433525D9F25240C183B064D8DC3A40
172	Nova Specialty Surgery	 J - 2/37, Mahaveer Marg, Opp Jai Club, C - Scheme , Jaipur 	1412370209	0101000020E6100000DE28684936F3524065F217C451E93A40
173	Metro Mas Hospital & Research Centre	 Shipra Path, Near Techonology Park, Mansarovar, Jaipur 	1412786001	0101000020E610000045EB4DB10FF15240A6E4E6655EDE3A40
174	Sevayatan Maternity & General Hospital	 Sodala,Ajmer Road , Jaipur 	1412220250	0101000020E61000000AE0C1AA55F15240D4FF8359B2E63A40
175	Tagore Hospital & Research Institute	 Tagore Lane, Sector 7, Shipra Path, Mansarovar, Jaipur 	1416450554	0101000020E61000000F07AE974BF15240C61858C7F1DB3A40
176	Deep Hospital & Research Centre	 Khatipura Road, Jhotawara, Jaipur 	1412466330	0101000020E6100000A0F60082DEF052404ECAEE1351F13A40
178	Janana Hospital	 86/208, Pratap Nagar, Kumbha Marg, Sanganaer, Jaipur 	1412790085	0101000020E6100000710D220846F45240ED2F16E181CC3A40
179	Asopa Hospital	 93 - B, Tagore Nagar, Ajmer Road, Near D.C.M, Jaipur 	1412246161	0101000020E6100000EE8623FE97EF52406C7FC23467E43A40
180	Sadhna Hospital	 Jharkhand Mod, Khatipura Road, Jaipur 	1413208180	0101000020E6100000B0C16790A7EE5240701EF3A6C7EB3A40
183	Roma Nursing	 Home Opp Dcm Shops, Ajmer Road, Jaipur 	1412811195	0101000020E6100000684F13C74BF15240C1069F419EE63A40
184	Bhatia Hospital	 Bhatia Bhawan, Panchwati Circle, Raja Park, Jawahar Nagar, Jaipur 	1412603837	0101000020E6100000E5B67D8FFAF4524027F56569A7E63A40
186	Jyoti Nursing Home	 Near Police Station, Road No. 4, Opp SBI Bank, V.K.I Area, Jaipur 	1414047144	0101000020E6100000BBC7E3B386F252400DBB39A638FE3A40
187	Dr. Chauhan Orthopaedic Centre	 44, Ambabari Petrol Pump, Jhotwara Road, Bani Park, Jaipur 	1412338349	0101000020E61000009B47B4D3C5F252402A768FC767EF3A40
188	Jain Fertlity & Mother Care Hospital	 Amrapali Marg, Vaishali, Hanuman Nagar , Jaipur 	1412359289	0101000020E610000075A213F879EF5240917241C758E93A40
190	Bansal Hospital & Research Centre	 17, Janak Puri -1, Imliwala Phatak, Barkat Nagar, Jaipur 	1412591110	0101000020E610000088EA52C8BAF25240D1B6F52E39E33A40
191	Sita Devi Hospital	 18, Nandpuri Extension, 80 Feet Road, Hawa Sadak, Jaipur 	1412210734	0101000020E6100000B4098BE5F1F15240ECD6D75F1BE63A40
192	Nirmala Hospital & Research Centre	 J - 52, Near Bagaria Bhawan, C Scheme, Krishna Marg, Jaipur 	1412376220	0101000020E610000084EDCCA989F35240BEC92544AFE93A40
193	Atal Hospital	 B - 494, Mahesh Nagar, 80 Feet Road, Jaipur 	1412502181	0101000020E610000038D0E8B328F2524077D03648D2E13A40
195	Shri Krishna Hospital	 1/31, Near Aggarwal Caterer, Vidhyadhar Nagar, Jaipur 	1412232312	0101000020E6100000F763496F6EF65240E0E46D5B4AD43A40
196	Dulet Hospital	 Sagar Colony, Aimer road, Bhankrota, Jaipur 	1412251187	0101000020E610000008C72C7B92EC524005C7C09547DE3A40
197	Archana Hospital	 Thana Circle, Behind Petrol Pump, Airport Colony, Shiv Colony, Jaipur Gpo, Jaipur 	1413295734	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
198	Rajasthan Unani Medical College & Hospital	 Paldi Meena, Agra Road, Jaipur 	1412680115	0101000020E61000008C2FDAE385F752403B32FBE18CE43A40
200	Saaol Heart Center	 21-D-1, Barwada House Colony , Near Ajmer Puli, Ajmer Road, Jaipur 	1414007141	0101000020E6100000684F13C74BF15240C1069F419EE63A40
201	Shekhawati Hospital & Research Centre	 A - 2, Opp. Time Square Central Spine, Vidhyadhar Nagar, Jaipur 	1412232211	0101000020E6100000B2309F07D2F152400FE8F120E2F53A40
202	Amar Jain Hosptial	 Chaura Rasta, Near State Bank, Jaipur 	1412313099	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
204	Bagadi Hospital	 Jhotwara, 53 - 54 B, Kalvas Road, Jaipur 	1412340677	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
205	Ram Avtar Eye Hospital	 & Glaucoma Pavilion C - 17, Near Lbs College,Bhagat Singh Marg, Tilak Nagar, Jaipur 	1412621448	0101000020E610000062516C6086F45240146289624DE63A40
206	Gem Hospital	 60/7, Rajat Path, New Sanganer Road, Mansarovar, Jaipur 	1412782069	0101000020E610000001CB0006B8F05240081D740987DA3A40
208	Mansarover Hospital & Maternity Centre	 111/196, Agarwal Farm, Vijay Path, Mansarovar, Jaipur 	1412396988	0101000020E6100000A53CA9E53DF152408705AD7603D93A40
209	Kandoi Hospital	 Mukund Pura Road, Bhakrota, Jaipur GPO, Jaipur 	1412250089	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
210	Aditi Eye Hospital	 A - 37 - 40, Chitrakoot Stadium,Vaishali Nagar, Jaipur 	1412440970	0101000020E610000080A9B063EDEF524031BC48579FE73A40
212	Sharda Nursing	 Home 39, C Scheme, R V Sarda Marg, Jaipur 	1412377665	0101000020E610000079831E20CEF25240EEC1B52451E93A40
213	Girdhar Hospital & Research Centre	 11/34,73, Girdhar Marg, Malviya Nagar, Jaipur 	1412552668	0101000020E61000000BFAC1AFC7F35240ED9DD15625D93A40
214	Usha Hospital & Infertility Centre	 A - 199, Khatipura, Hanuman Nagar, Dashrath Marg, Jhotwara, Jaipur 	1412350763	0101000020E6100000A5AF31F9B0EF5240266DAAEE91EB3A40
216	Wadhwani Hospital & Eye Centre	 37, Near Dcm Godam, Ajmer Road, Mahatma Gandhi Nagar, Jaipur 	1412351315	0101000020E6100000B6A79A0FBEEF52403EB6C079CCE33A40
217	Anupam Hospital	 17, Dayal Nagar, Near Narayan Niwas, Durgapura, Jaipur 	1412502766	0101000020E610000094516518F7F152404C022F8E25DF3A40
218	Adam Shah Hospital	 Ansariya Welfare Society, Ghat Gate, Jaipur 	1412615629	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
219	Chandrabhan Hospital	 D -18, Sunder Vihar, Swej Farm, Near Jodhpur Misthan Bhandar, Sodala, Jaipur 	1412297233	0101000020E6100000D781188D21F152409232F32E28E73A40
220	Malhotra Family Hospital	 4/5, Malviya Nagar, Jaipur 	1412521032	0101000020E61000000A99D0C9F7F25240C855760B6ED93A40
221	Lucky Hospital	 Lakshy Bhawan, Muhana, Balaji Market, Ap Sanganaer, Jaipur 	1413294207	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
223	Bhiwani Eye Hospitals	 434, Telipara Chauraha Rasta, Bees Dukan, Adarsh Nagar, Jaipur 	1412575249	0101000020E6100000312B6FA2F1F45240A05225CADEE63A40
224	Nalini Hospital	 E - 19, New Light Colony, Tonk Road, Jaipur Diary Over Bridge, Jaipur 	1412553131	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
226	Rama Krishna Eye Hospital	 Aggarwal Farm, 3/25, SFS, Mansarovar, Jaipur 	1412395015	0101000020E6100000CBCD812F96F152401672005878D63A40
227	Sun Hospital & Maternity Centre	 J P Colony, Main Road, Tonk Pathak, Jaipur 	1412592063	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
228	Pandya Hospital & Research Centre	 J - 2/37, Opp JAI Club, C Scheme, Mahaveer Marg, Jaipur 	1412370209	0101000020E610000099A322A9E0F352405DA3E5400FE93A40
229	Bani Park Heart & General Hospital	 D - 9, Kabir Marg, Bani Park, Jaipur 	1412203864	0101000020E61000008E0244C18CF2524009CECFC3BFEC3A40
230	Vaishali Hospital & Surgical Research Centre	 B - 283, Vaishali Nagar, Jaipur 	1413100196	0101000020E610000080A9B063EDEF524031BC48579FE73A40
231	Vaishali Hospital & Surgical Research Centre	 B - 283, Vaishali Nagar, Jaipur 	1413100196	0101000020E610000080A9B063EDEF524031BC48579FE73A40
233	Khetan Hospital	 Near Murlipura Scheme, Sikar Road, Dher Ka Balaji, Jaipur 	1412230476	0101000020E6100000F061404008F1524099219FA1C9F13A40
234	Bharat Hospital	 Khole Ke Hanuman Ji, Delhi Road, Jaipur City, Jaipur 	1412634794	0101000020E61000006A5E1FE7C7F452400E5D9A6C4DDA3A40
236	Kedawat Hospital	 12 Mile, Tonk Road, Jaipur	141302022	0101000020E610000050453BB805F45240CF09EB1049CC3A40
237	Pragati Orthopaedic Hospital	 B-10, Janta Colony, Jaipur 	1412600326	0101000020E6100000D5FE733D76F5524091C5EC1B4EE73A40
238	R P Memorial Hospital	 5 Tha 8, Jawahar Nagar, Jaipur 	1412653689	0101000020E61000003B10A33124F5524006F357C85CE33A40
239	Kanodia Hospital	 C-256, Hans Marg, Malviya Nagar, Jaipur 	1412524624	0101000020E61000006E01FCAEE3F352408319AE6939DB3A40
240	Prerna Hospital	 126, Krishna Nagar, Teen Dukan Dher Ke Balaji, Sikar Road, Jhotwara, Jaipur 	1412232136	0101000020E6100000C9F1F917F7F152401F6D776A89F03A40
241	Anil Hospital	 C - 13, Deepak Marg, M D Road, Jaipur 	1412601371	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
243	Santoshi Hospital	 B - 276, Vaishali Circle, Vaishali Nagar, Jaipur 	1412350782	0101000020E6100000F36ED16E99EF5240C9B6C2AA30EA3A40
244	Anshu Hospital	 B - 209, Near Maharani Apt, Rajendra Marg, Bapu Nagar, Jaipur 	1412710801	0101000020E6100000BF7CB262B8F3524082F9D0AA3BE33A40
245	Tandon Eye Centre	 5,Achrol House, Near Ajmer Pulia, Civil Lines, Jaipur 	1412222855	0101000020E6100000AA0F7FA80FF25240B258D47146E83A40
246	JN Mishra Hospital	 B - 52, Gautam Marg, Vaishali Nagar, Jaipur 	1412357763	0101000020E610000053AB54F597EF5240B328ECA2E8E93A40
1	Sardar Mal Khandaka Memorial Hospital	Gram Hatoj, Kalwar Road, Jaipur, Rajasthan - 302012	1412860113	0101000020E61000003423CD69E0ED524011A96917D3F23A40
4	Jyoti Hospital	Shri Ram Colony, Near Loha Mandi, Sanshar Chadra Road, Jaipur, Rajasthan - 302001	0	0101000020E610000087BAB486F77E5340678984A570313B40
8	Homeopathic Hospital	Fire Stations, Sitapura, Mahatma Gandhi Marg, Sitapur Industrial Area, Jaipur, Rajasthan - 302022	1412771009	0101000020E610000080046AD63EF65240BA04F1CBAAC73A40
15	Mahatma Gandhi Cardiac and Critical Care Center	RIICO Institutional Area Sitapura, Sitapur Industrial Area, Jaipur, Rajasthan - 302022	9799035131	0101000020E61000006FFE15E8C9F652401E92B5E10BC43A40
21	National Hospital	Sanjay Bazaar, Ghat Darwaza, Jaipur, Rajasthan - 302003	1412574490	0101000020E61000008AF3812E2BF55240E52A16BF29EA3A40
23	Sunrise Naturopathy & Ayurvedic Hospital And Yoga Center	Village Sar (Bilochi), Delhi-Chandwaji-Ajmer Bypass , Express Highway, Jaipur, Rajasthan - 302026	1423514101	0101000020E610000096B20C71ACFC5240259D37047D3A3B40
31	Radiant Hospital & Urological Research Institute	A - 4 - B, Opp Udyog Bhawan, C Scheme, Tilak Marg, Jaipur, Rajasthan - 302001	1412225475	0101000020E6100000A5BDC11726F352403D93A23AC2E63A40
37	Pradeep Hospital	Chandi Ki Taksal, Tripolia Bazar, Jaipur, Rajasthan - 302002	1412617649	0101000020E6100000C11DA8539EF452401A530B804CEE3A40
40	Daksh Ayurveda Clinic & Panchakarma Centre	GF-1, Jain Kunj, Gopalbadi, Ajmer Road, Jaipur, Rajasthan - 302001	1414001764	0101000020E610000069EBE060EFF25240C41FEAE346EA3A40
44	Kshitij Hospital & Medical Research Center	52, Near Sushma Gas Godown, Near Kisan Dharmkanta, Uday Nagar - A, Gopalpura Bypas, Jaipur, Rajasthan - 302018	1412812121	0101000020E6100000E4446051B6F2524033A083D327DA3A40
52	Jaimahal Hospital	A - 24, Bibi Fatima Colony, Ramgarh MOD, Najafgarh Road, Tripolia Bazar, Jaipur, Rajasthan - 302002	0	0101000020E6100000C11DA8539EF452401A530B804CEE3A40
57	Rajgirish Hospital	Mahapura Bus Stand, Ajmer Road, Jaipur, Rajasthan - 302006	0	0101000020E610000068E498D1B4F15240F1E2B32BE3E73A40
61	N. K. Dental Hospital	11, Greater Kailash Colony, Opp. Petrol Pump, Lalkothi, Jaipur, Rajasthan - 302015	1412741641	0101000020E6100000FD4003FB79F352400F0FBCB5A7E33A40
66	Saville Hospital & Research Centre	Sec - 5, Shipra Path, Mansarovar, Jaipur, Rajasthan - 302020	0	0101000020E610000059DB148F0BF15240DEF64E1608DE3A40
70	Liberty Hospital	115, Tagore Path, Sector - 11, Agarwal Farm, Mansarovar, Jaipur, Rajasthan - 302020	9414074818	0101000020E6100000C50E194545F15240AD68739CDBDA3A40
76	Jaipur Ayurveda Hospital	A-49, Jai Ambey Nagar, Near Gopalpura Flyover, Opp. Mahaveer Nagar, Tonk Road, Jaipur, Rajasthan - 302015	1412724072	0101000020E61000006BF8CC48F3F2524049C961E6F1DC3A40
84	Gautam Hospital & Research Center	Civil Lines, 1, Jacob Road, Jaipur, Rajasthan - 302006	1412222111	0101000020E61000008772FD1678F2524067542AF235E93A40
89	Jiva Ayurveda Clinic	B-5, Subhashnagar shopping arcade, Near  TB hospital, Shastrinagar, Jaipur, Rajasthan - 302016	1414025412	0101000020E6100000BBC7E3B386F25240276728EE78EF3A40
92	Amarnath Allergy & General Hospital	Near Police Chowki, Gopalpura By Pass Road, Jaipur GPO, Jaipur, Rajasthan - 302001	1414346051	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
98	Ped Gastro Clinic	A 166, Ramnagariya JDA scheme, Near SKIT engineering college, Jagatpura, Jaipur, Rajasthan - 302017	1412759981	0101000020E6100000914CE207C2F452406479B2F6D2D53A40
104	Dr. Jitendra Singh Makkar	Room No - 4016, Cardiac OPD, Fortis Escorts Hospital, Jawahar Lal Nehru Marg, Malviya Nagar, Jaipur, Rajasthan - 302017	1412547000	0101000020E610000068CFC02385F352407CA477E052DA3A40
109	Pradeeep Rawal Memorial Hospital	In Front Of Govind DEV JEE Tpl, Badi Chaupar, Tripolia Bazar, Jaipur, Rajasthan - 302002	1412611295	0101000020E6100000F8CF89F3A6F4524072F2C7597DEC3A40
114	Poorva Hospital	164, Heera Nagar, Dcm, Ajmer Road, Jaipur, Rajasthan - 302001	1412354400	0101000020E610000069EBE060EFF25240C41FEAE346EA3A40
117	Saraswat Dental Hospital	D - 6/404, Near State Bank Of Bikaner, Vaishali Nagar, Jaipur, Rajasthan - 302021	1412351728	0101000020E610000080A9B063EDEF524031BC48579FE73A40
131	Sharda General Hospital	112/15, Arawal Farm, Thadi Market, Madhyam Marg, Jaipur GPO, Jaipur, Rajasthan - 302001	1412398826	0101000020E6100000BDD584FED3F352405C06F75ED7EA3A40
140	Narayana Multispeciality	 Hospital Kumbha Marg, Pratap Nagar, Sanganar, Jaipur 	1415192939	0101000020E610000056C2C9EC62F25240AED4B32094E93A40
145	Jain ENT Hospital	 23 Satya Vihar Colony, Pankaj Singhavi Marg, Near Vidhan Sabha, Lal Kothi, Jaipur 	1412742828	0101000020E6100000363F598C15F35240B3BC06D8A2E43A40
150	Monilek Hospital & Research Centre	 Sector 4, Jawahar Nagar, Jaipur 	1412653019	0101000020E61000004198DBBD5CF55240867C2B6DCCE23A40
153	S.R. Kalla Hospital	 78, Dhuleshwar Garden, Behind HSBC Bank, Sardar Patel Marg, C - Scheme, Jaipur 	1415112042	0101000020E610000090FA46CF08F352405E6743FE99E93A40
159	Jain Eye Clinic & Hospital	 K-4-A, Fatehtiba, Moti Doongri Road, Adarsh Nagar, Jaipur 	1412611211	0101000020E6100000310FF4AB94F452401C4E4F0E55E73A40
163	Shivani Fertility and Mother Care	 Sec -10, Aggarwal Farm, Meera Marg, Mansarovar, Jaipur 	1412785075	0101000020E6100000862D872705F152408A90BA9D7DD93A40
168	Government Satellite Hospital	 B-5, Shiv Marg,  Bani Park, Jaipur 	1412202449	0101000020E610000091BF6A1B35F35240D303D5F5FAED3A40
171	Allergy Asthma & Chest Hospital	 S - 92, Behind Jaipur Hospital, Tonk Road, Mahaveer Nagar, Jaipur 	1412553122	0101000020E61000008C09D6DDE1F252405477C0D07EDB3A40
177	Goyal Hospital	 Shashtri Nagar, A - 1, Near Police Station, Jaipur 	1412280940	0101000020E6100000D2167C3F5AF3524098744AF6BEF33A40
181	Rajdhani Hospital	 C - 30, Bhagwan Das Road,C Scheme, Jaipur 	1412371202	0101000020E61000008DB85BEDBCF352402B8716D9CEE93A40
182	Saket Hospital	 Sector 10 Meera Marg, Agarwal Farm, Mansarovar, Jaipur 	1412785075	0101000020E6100000862D872705F152408A90BA9D7DD93A40
185	Meera Dental Hospital	 B -13, Kartavya, Shiv Marg, Bani Park, Jaipur 	1412202220	0101000020E610000091BF6A1B35F35240D303D5F5FAED3A40
189	Indowestern Brain & Spine Hospital	 C - 18, Near New Vidhan Sabha, LAL Kothi, Jaipur 	1412744441	0101000020E6100000B204638E2FF35240B6EA84E16DE53A40
249	Shubam Hospital	 A - 35, Vidhyut Nagar Crossing, Ajmer Road, Jaipur 	1412245987	0101000020E6100000684F13C74BF15240C1069F419EE63A40
250	Shri Brij Fracture Hospital	 Ajay Circle, Mansarover Colony, Near Mathur Farm, Factory Area Road, Kalwar Road, Jhotwara, Jaipur 	1412349234	0101000020E61000004322C89C42EF5240DEBC827EF0F13A40
251	Poona Udawat Memorial Eye Hospital	 A - 93, Shiv Marg, Shyamnagar, Jaipur 	1413117272	0101000020E6100000FA7953910AF35240A3247F8BA9ED3A40
253	Ginni Devi Memorial Hospital & Fracture Clinic	 94/49 - 50, Agarwal Farm, Gokhe Marg, Mansarovar, Jaipur 	1412396898	0101000020E610000000C22C593AF1524045F9DDCF84D93A40
254	Jal Mahal Hospital	 A - 24, Bibi Fatima Colony, Ram Garh Road, Tripolia Bazar, Jaipur 	1412630931	0101000020E61000003419445A19F6524070B378B130F23A40
255	Sonkhiya Hospital	 A - 53 - A, Near Jain Mandir Kanwtia Circle, Shastri Nagar, Jaipur 	1412300534	0101000020E61000005FBAA4C574F35240543882548AF33A40
256	Jolly Hospital	 B - 9d, Govind Marg, Adarsh Nagar, Jaipur 	1412612540	0101000020E610000059F5B9DA8AF55240125553ED2EE73A40
194	RKV Maxima Heart Institute & Reaserch Center	 D - 78, Bani Park, Ghiya MG, Bani Park, Jaipur 	1412281226	0101000020E61000004F06A2829EF2524072E0D57267EE3A40
199	Life Care Hospital	 119, Keshav Vihar, Gopalpura Bypass, Jaipur 	1412762587	0101000020E61000007F791222AAF1524046C71F56C9DF3A40
203	Dr.K.D. Gupta Medical & Dental Centre	 16, Hospital Road, C-Scheme, Jaipur 	1412368150	0101000020E6100000C04AF4E8FCF3524001F099362DE83A40
207	Mangalam Hospital	 C - 30a, Baraf Khana, Adarsh Nagar, Jaipur 	1412615367	0101000020E6100000312B6FA2F1F45240A05225CADEE63A40
211	Shri Navkar Hospital & Maternity Centre	 G - 11, 3rd Floor, Shubham Apartment, Central Spine, Vidhyadhar Nagar, Jaipur 	1412338257	0101000020E6100000682101A3CBF152401D739EB12FF53A40
215	Rajputana Hospital & Research Centre	 A - 16, Khatipura, Sirsi Tiraha, Hospital Road, Sirsi Road, Hanuman Nagar, Jaipur 	1414022001	0101000020E61000000EE9013D9EEF52402ACB6B802DEC3A40
222	Nahar Hospital	 K - I, Income Tax Colony, Durgapura, Tonk Road, Barkat Nagar, Jaipur 	1412545830	0101000020E6100000908F7120FFF25240BCA47BE761DE3A40
225	Pinkcity Heart & General Hospital	 34, Opposite Road No - 1, Murlipura, Shankar Nagar Road, Vishwakarma Industrial Area, Jaipur 	1412332640	0101000020E6100000BBC7E3B386F252400DBB39A638FE3A40
232	Holy Family Hospital	 337/5, Opp Ac Market, Rajapark, Jawahar Nagar, Jaipur	141262210	0101000020E610000062F7787CD6F45240F5FCC45CADE43A40
235	Global Heart & General Hospital	 C - I 27 - 29, Chitrakoot, Opposite Bharat Apartment, Gandhi Path, Ajmer Road, Vaishali Nagar, Jaipur 	1414006290	0101000020E6100000B6A79A0FBEEF52403EB6C079CCE33A40
242	Little Home Children Hospital & Research Centre	 C - 62, LAL Kothi Scheme, LAL Kothi, Jaipur 	1412742726	0101000020E61000000770C4A409F352403596557D53E33A40
247	Puru Eye Clinic	 Shop No. 25, Opp. Pani Ki Tanki, Tonk Road, Durgapura, Jaipur 	1413213150	0101000020E61000005D990178D5F252402A4A534EC5D93A40
248	Anand Maternity Hospital	 Raghunath Puri, Kalwar Road, Sriram Path, Jhotwara, Jaipur 	1412343390	0101000020E61000008E5E68098DEF524063450DA661F43A40
252	T K Hospital & Diagnostic Centre	 Near Sanganeri Gate, M D Road, Moti Doongri Road, Jaipur 	1412615292	0101000020E610000059405711A4F45240E014B1E31AE83A40
257	Solomon Hospital	 E - 19, Siddharth Nagar, Sector - 9, Main Road, Gandhi Marg, Malviya Nagar, Jaipur 	1412550078	0101000020E61000004082870E26F45240BE93050253E23A40
\.


--
-- Name: hospitals_hospital_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('hospitals_hospital_id_seq', 257, true);


--
-- PostgreSQL database dump complete
--

