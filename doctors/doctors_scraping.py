from models import Doctor,Appointment,Speciality,Expertise,Education,Award,Paper,Membership,Research,Practice,News,Contact

import requests,lxml
from lxml import html

def run(verbose=True):
	print 'Started Scraping Sehat.com Doctors'
	for i in range(185580,190000):
	# for i in range(1,2):
		try:
			r = requests.get('http://www.sehat.com/doctors-in-hyderabad/dr-jagarlapudi-murali-krishna-murthy/'+str(i)+'/1/')
			root = lxml.html.fromstring(r.content)

			doctor = Doctor()
			doctor.profile_url = r.url
			print 'Scraping: ' + r.url
			# doctor title
			tmp = root.cssselect('h1')
			if(tmp):
				doctor.title = tmp[0].text_content().strip()
			# doctor name
			tmp = root.cssselect('span.se_content_mid span.org')
			if(tmp):
				doctor.name = tmp[0].text_content().strip()
			# doctor description and languages
			tmp = root.cssselect('span.se_content_mid')
			if(tmp):
				tmp = tmp[0].text_content().strip()
				tmp_list = tmp.split('\r\n')
				tmp_list_length = len(tmp_list)
				# Stripping each element using for loop
				for i in range(tmp_list_length):
					tmp_list[i] = tmp_list[i].strip()
				doctor.description = tmp_list[2]
				doctor.languages = tmp_list[3]
			# doctor website
			tmp = root.cssselect("span.se_content_mid.extended_block a[rel='nofollow']")
			tmp_website = ""
			for t in tmp:
				tmp_website += ","
				tmp_website += t.text_content().strip()
			doctor.website = tmp_website
			# doctor phone
			tmp = root.cssselect('ul.se_article_info li')
			if(tmp):
				doctor.phone = tmp[0].text_content().strip()
			# doctor about
			tmp = root.cssselect('div.tab_div_i > ul')
			if(tmp):
				tmp = tmp[0].cssselect('li')
				tmp_about = ""
				for t in tmp:
					tmp_about += " "
					tmp_about += t.text_content().strip()
				doctor.about = tmp_about
			doctor.save()

			appointment = Appointment()
			appointment.doctor = doctor
			# Appointment: hospital name and phone
			tmp = root.cssselect('ul.se_article_info > li > table')
			if(tmp):
				tmp = tmp[0].cssselect('td')
				try:
					appointment.hospital_name = tmp[0].text_content().strip()
				except IndexError:
					print 'Appointment: Hospital Name Not Found'
				try:
					appointment.phone = tmp[2].text_content().strip()
				except IndexError:
					print 'Appointment: Hospital Phone Not Found'
			appointment.save()

			# practices
			tmp_list = root.cssselect('ul.se_article_list span.se_content_mid.extended_block')
			for tmp in tmp_list:
				practice = Practice()
				practice.doctor = doctor
				# practice hospital name
				try:
					practice.hospital_name = tmp.cssselect('span.org')[0].text_content().strip()
				except IndexError:
					print 'Practice: Hospital Name Not Found'
				# practice address
				a_list = tmp.text_content().strip()
				try:
					tmp_address = ""
					tmp_address += a_list[3]
					tmp_address += a_list[4]
					practice.address = tmp_address
				except IndexError:
					print 'Practice: Address Not Found'
				# Other Fields
				temp = tmp.cssselect('ul.se_article_info')
				if(temp):
					temp = temp[0].cssselect('li')
					# enquiries
					try:
						practice.enquiries = temp[0].text_content().strip()
					except IndexError:
						print 'Enquiries Not Found'
					# appointment
					try:
						practice.appointment = temp[1].text_content().strip()
					except IndexError:
						print 'Appointment Not Found'
					# emergency
					try:
						practice.emergency = temp[2].text_content().strip()
					except IndexError:
						print 'Emergency Not Found'
					# billing
					try:
						practice.billing = temp[3].text_content().strip()
					except IndexError:
						print 'Billing Not Found'
					# fax
					try:
						practice.fax = temp[4].text_content().strip()
					except IndexError:
						print 'Fax Not Found'
					# website
					try:
						practice.website = temp[5].text_content().strip()
					except IndexError:
						print 'Website Not Found'
				practice.save()

			contact = Contact()
			contact.doctor = doctor
			tmp = tmp.cssselect('ul.se_article_info')
			if(tmp):
				try:
					tmp = tmp[1].cssselect('li')
					# appointment
					try:
						contact.appointment = tmp[0].text_content().strip()
					except IndexError:
						print 'Contact: Appointment No Not Found'
					# consultation
					try:
						contact.consultation = tmp[2].text_content().strip()
					except IndexError:
						print 'Contact: Consultation No Not Found'
				except IndexError:
					print 'Something went wrong in contacts'
			contact.save()

			tmp = root.cssselect('div.tab_div_i > ul')
			if(tmp):
				# Speciality
				try:
					temp = tmp[1]
					temp = temp.cssselect('li')
					for t in temp:
						speciality = Speciality()
						speciality.doctor = doctor
						speciality.name = t.text_content().strip()
						speciality.save()
				except IndexError:
					print 'Speciality Not Found'
				# Expertise
				try:
					temp = tmp[2]
					temp = temp.cssselect('li')
					for t in temp:
						expertise = Expertise()
						expertise.doctor = doctor
						expertise.name = t.text_content().strip()
						expertise.save()
				except IndexError:
					print 'Expertise Not Found'
				# Education
				try:
					temp = tmp[3]
					temp = temp.cssselect('li')
					for t in temp:
						education = Education()
						education.doctor = doctor
						education.name = t.text_content().strip()
						education.save()
				except IndexError:
					print 'Education Not Found'
				# Research
				try:
					temp = root.cssselect('div#research ul li')
					for t in temp:
						research = Research()
						research.doctor = doctor
						research.name = t.text_content().strip()
						research.save()
				except IndexError:
					print 'Research Not Found'
				# Award
				try:
					temp = root.cssselect('div#awards ul li')
					for t in temp:
						award = Award()
						award.doctor = doctor
						award.name = t.text_content().strip()
						award.save()
				except IndexError:
					print 'Award Not Found'
				# Paper
				try:
					temp = root.cssselect('div#ppt ul li')
					for t in temp:
						paper = Paper()
						paper.doctor = doctor
						paper.name = t.text_content().strip()
						paper.save()
				except IndexError:
					print 'Paper Not Found'
				# Membership
				try:
					temp = root.cssselect('div#memberships ul li')
					for t in temp:
						membership = Membership()
						membership.doctor = doctor
						membership.name = t.text_content().strip()
						membership.save()
				except IndexError:
					print 'Membership Not Found'
				# News
				try:
					temp = root.cssselect('div#news ul li')
					for t in temp:
						news = News()
						news.doctor = doctor
						news.name = t.text_content().strip()
						news.save()
				except IndexError:
					print 'News Not Found'
			print 'Saved Data for url: ' + r.url
		except:
			print 'Redirect Loop May be or Unknown Error'
	print 'Scraping Finished'
