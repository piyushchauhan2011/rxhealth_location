from django.contrib.gis import admin
from models import Doctor,Appointment,Speciality,Expertise,Education,Award,Paper,Membership,Research,News,Practice,Contact

# Register your models here.
admin.site.register(Doctor)
admin.site.register(Appointment)
admin.site.register(Speciality)
admin.site.register(Expertise)
admin.site.register(Education)
admin.site.register(Award)
admin.site.register(Paper)
admin.site.register(Membership)
admin.site.register(Research)
admin.site.register(News)
admin.site.register(Practice)
admin.site.register(Contact)