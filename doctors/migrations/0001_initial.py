# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Doctor'
        db.create_table(u'doctors_doctor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('languages', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('website', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('about', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'doctors', ['Doctor'])

        # Adding model 'Appointment'
        db.create_table(u'doctors_appointment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['doctors.Doctor'])),
            ('hospital_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'doctors', ['Appointment'])

        # Adding model 'Speciality'
        db.create_table(u'doctors_speciality', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['doctors.Doctor'])),
            ('name', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'doctors', ['Speciality'])

        # Adding model 'Expertise'
        db.create_table(u'doctors_expertise', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['doctors.Doctor'])),
            ('name', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'doctors', ['Expertise'])

        # Adding model 'Education'
        db.create_table(u'doctors_education', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['doctors.Doctor'])),
            ('name', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'doctors', ['Education'])

        # Adding model 'Award'
        db.create_table(u'doctors_award', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['doctors.Doctor'])),
            ('name', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'doctors', ['Award'])

        # Adding model 'Paper'
        db.create_table(u'doctors_paper', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['doctors.Doctor'])),
            ('name', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'doctors', ['Paper'])

        # Adding model 'Membership'
        db.create_table(u'doctors_membership', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['doctors.Doctor'])),
            ('name', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'doctors', ['Membership'])

        # Adding model 'Research'
        db.create_table(u'doctors_research', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['doctors.Doctor'])),
            ('name', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'doctors', ['Research'])

        # Adding model 'News'
        db.create_table(u'doctors_news', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['doctors.Doctor'])),
            ('link', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'doctors', ['News'])

        # Adding model 'Practice'
        db.create_table(u'doctors_practice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('doctor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['doctors.Doctor'])),
            ('hospital_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('enquiries', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('appointment', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('emergency', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('billing', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('website', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal(u'doctors', ['Practice'])


    def backwards(self, orm):
        # Deleting model 'Doctor'
        db.delete_table(u'doctors_doctor')

        # Deleting model 'Appointment'
        db.delete_table(u'doctors_appointment')

        # Deleting model 'Speciality'
        db.delete_table(u'doctors_speciality')

        # Deleting model 'Expertise'
        db.delete_table(u'doctors_expertise')

        # Deleting model 'Education'
        db.delete_table(u'doctors_education')

        # Deleting model 'Award'
        db.delete_table(u'doctors_award')

        # Deleting model 'Paper'
        db.delete_table(u'doctors_paper')

        # Deleting model 'Membership'
        db.delete_table(u'doctors_membership')

        # Deleting model 'Research'
        db.delete_table(u'doctors_research')

        # Deleting model 'News'
        db.delete_table(u'doctors_news')

        # Deleting model 'Practice'
        db.delete_table(u'doctors_practice')


    models = {
        u'doctors.appointment': {
            'Meta': {'object_name': 'Appointment'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            'hospital_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'doctors.award': {
            'Meta': {'object_name': 'Award'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'doctors.doctor': {
            'Meta': {'object_name': 'Doctor'},
            'about': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'languages': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'doctors.education': {
            'Meta': {'object_name': 'Education'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'doctors.expertise': {
            'Meta': {'object_name': 'Expertise'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'doctors.membership': {
            'Meta': {'object_name': 'Membership'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'doctors.news': {
            'Meta': {'object_name': 'News'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'doctors.paper': {
            'Meta': {'object_name': 'Paper'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'doctors.practice': {
            'Meta': {'object_name': 'Practice'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'appointment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'billing': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            'emergency': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'enquiries': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'hospital_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'doctors.research': {
            'Meta': {'object_name': 'Research'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'doctors.speciality': {
            'Meta': {'object_name': 'Speciality'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True'})
        }
    }

    complete_apps = ['doctors']