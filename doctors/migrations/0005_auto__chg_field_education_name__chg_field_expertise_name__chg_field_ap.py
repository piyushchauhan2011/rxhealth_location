# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Education.name'
        db.alter_column(u'doctors_education', 'name', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Expertise.name'
        db.alter_column(u'doctors_expertise', 'name', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Appointment.hospital_name'
        db.alter_column(u'doctors_appointment', 'hospital_name', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'Membership.name'
        db.alter_column(u'doctors_membership', 'name', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Contact.consultation'
        db.alter_column(u'doctors_contact', 'consultation', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'News.link'
        db.alter_column(u'doctors_news', 'link', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'Practice.hospital_name'
        db.alter_column(u'doctors_practice', 'hospital_name', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'Award.name'
        db.alter_column(u'doctors_award', 'name', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Doctor.name'
        db.alter_column(u'doctors_doctor', 'name', self.gf('django.db.models.fields.CharField')(max_length=255))

        # Changing field 'Paper.name'
        db.alter_column(u'doctors_paper', 'name', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Speciality.name'
        db.alter_column(u'doctors_speciality', 'name', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Research.name'
        db.alter_column(u'doctors_research', 'name', self.gf('django.db.models.fields.TextField')())

    def backwards(self, orm):

        # Changing field 'Education.name'
        db.alter_column(u'doctors_education', 'name', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Expertise.name'
        db.alter_column(u'doctors_expertise', 'name', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Appointment.hospital_name'
        db.alter_column(u'doctors_appointment', 'hospital_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Membership.name'
        db.alter_column(u'doctors_membership', 'name', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Contact.consultation'
        db.alter_column(u'doctors_contact', 'consultation', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'News.link'
        db.alter_column(u'doctors_news', 'link', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Practice.hospital_name'
        db.alter_column(u'doctors_practice', 'hospital_name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Award.name'
        db.alter_column(u'doctors_award', 'name', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Doctor.name'
        db.alter_column(u'doctors_doctor', 'name', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

        # Changing field 'Paper.name'
        db.alter_column(u'doctors_paper', 'name', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Speciality.name'
        db.alter_column(u'doctors_speciality', 'name', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Research.name'
        db.alter_column(u'doctors_research', 'name', self.gf('django.db.models.fields.TextField')(null=True))

    models = {
        u'doctors.appointment': {
            'Meta': {'object_name': 'Appointment'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            'hospital_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'doctors.award': {
            'Meta': {'object_name': 'Award'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'doctors.contact': {
            'Meta': {'object_name': 'Contact'},
            'appointment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'consultation': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'doctors.doctor': {
            'Meta': {'object_name': 'Doctor'},
            'about': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'languages': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'profile_url': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'doctors.education': {
            'Meta': {'object_name': 'Education'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'doctors.expertise': {
            'Meta': {'object_name': 'Expertise'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'doctors.membership': {
            'Meta': {'object_name': 'Membership'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'doctors.news': {
            'Meta': {'object_name': 'News'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'})
        },
        u'doctors.paper': {
            'Meta': {'object_name': 'Paper'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'doctors.practice': {
            'Meta': {'object_name': 'Practice'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'appointment': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'billing': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            'emergency': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'enquiries': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'hospital_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'doctors.research': {
            'Meta': {'object_name': 'Research'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'doctors.speciality': {
            'Meta': {'object_name': 'Speciality'},
            'doctor': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['doctors.Doctor']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'default': "''"})
        }
    }

    complete_apps = ['doctors']