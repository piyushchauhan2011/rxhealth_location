from django.contrib.gis.db import models

# Create your models here.
class Doctor(models.Model):
	title = models.CharField(max_length=255,null=True)
	name = models.CharField(max_length=255,default="")
	languages = models.CharField(max_length=255,null=True)
	website = models.CharField(max_length=255,null=True)
	phone = models.CharField(max_length=255,null=True)
	about = models.TextField(null=True)
	description = models.CharField(max_length=255,null=True)
	profile_url = models.CharField(max_length=255,null=True)

	def __unicode__(self):
		return self.name

class Appointment(models.Model):
	doctor = models.ForeignKey(Doctor)
	hospital_name = models.CharField(max_length=255,default="")
	phone = models.CharField(max_length=255,null=True)

	def __unicode__(self):
		return self.hospital_name

class Speciality(models.Model):
	doctor = models.ForeignKey(Doctor)
	name = models.TextField(default="")

	def __unicode__(self):
		return self.name

class Expertise(models.Model):
	doctor = models.ForeignKey(Doctor)
	name = models.TextField(default="")

	def __unicode__(self):
		return self.name

class Education(models.Model):
	doctor = models.ForeignKey(Doctor)
	name = models.TextField(default="")

	def __unicode__(self):
		return self.name

class Award(models.Model):
	doctor = models.ForeignKey(Doctor)
	name = models.TextField(default="")

	def __unicode__(self):
		return self.name

class Paper(models.Model):
	doctor = models.ForeignKey(Doctor)
	name = models.TextField(default="")

	def __unicode__(self):
		return self.name

class Membership(models.Model):
	doctor = models.ForeignKey(Doctor)
	name = models.TextField(default="")

	def __unicode__(self):
		return self.name

class Research(models.Model):
	doctor = models.ForeignKey(Doctor)
	name = models.TextField(default="")

	def __unicode__(self):
		return self.name

class News(models.Model):
	doctor = models.ForeignKey(Doctor)
	link = models.CharField(max_length=255,default="")

	def __unicode__(self):
		return self.link

class Practice(models.Model):
	doctor = models.ForeignKey(Doctor)
	hospital_name = models.CharField(max_length=255,default="")
	address = models.CharField(max_length=255,null=True)
	enquiries = models.CharField(max_length=255,null=True)
	appointment = models.CharField(max_length=255,null=True)
	emergency = models.CharField(max_length=255,null=True)
	billing = models.CharField(max_length=255,null=True)
	fax = models.CharField(max_length=255,null=True)
	website = models.CharField(max_length=255,null=True)

	def __unicode__(self):
		return self.hospital_name

class Contact(models.Model):
	doctor = models.ForeignKey(Doctor)
	appointment = models.CharField(max_length=255,null=True)
	consultation = models.CharField(max_length=255,default="")

	def __unicode__(self):
		return self.consultation