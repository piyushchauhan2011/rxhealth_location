from django.contrib.gis.db import models

# Create your models here.
class MedicalStore(models.Model):
	name = models.CharField(max_length=255,default="")
	address = models.CharField(max_length=255,default="")
	phone = models.CharField(max_length=255,default="")
	link = models.CharField(max_length=255,default="")
	city = models.CharField(max_length=255,default="")
	point = models.PointField(null=True)

	objects = models.GeoManager()

	def __unicode__(self):
		return self.name
