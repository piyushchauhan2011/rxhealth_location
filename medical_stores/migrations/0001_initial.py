# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MedicalStore'
        db.create_table(u'medical_stores_medicalstore', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('address', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('phone', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('link', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('city', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('point', self.gf('django.contrib.gis.db.models.fields.PointField')(null=True)),
        ))
        db.send_create_signal(u'medical_stores', ['MedicalStore'])


    def backwards(self, orm):
        # Deleting model 'MedicalStore'
        db.delete_table(u'medical_stores_medicalstore')


    models = {
        u'medical_stores.medicalstore': {
            'Meta': {'object_name': 'MedicalStore'},
            'address': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'city': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'phone': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {'null': 'True'})
        }
    }

    complete_apps = ['medical_stores']