from models import MedicalStore

import requests,lxml
from lxml import html

# Delhi NCR and Jaipur Scraped
# Others Remaining

def run(verbose=True):
	# Highlighted Ones Scraping
	# print 'Highlighted Ones Started Scraping'
	# r = requests.get('http://jaipur.yellowpages.co.in/medical+store?trc=231&page=1')
	# root = lxml.html.fromstring(r.content)

	# tmp_list = root.cssselect('div.MT_20 .MT_20')
	# for tmp in tmp_list:
	# 	medical_store = MedicalStore()
	# 	medical_store.name = tmp.cssselect('div.FL h2.listingName')[0].text_content().strip()
	# 	medical_store.address = tmp.cssselect('div.FL p.location')[0].text_content().strip()
	# 	medical_store.phone = tmp.cssselect('div.FL div.phoneDetails')[0].text_content().strip()
	# 	medical_store.link = tmp.cssselect('a')[0].get('href')
	# 	medical_store.city = 'Jaipur'
	# 	medical_store.save()
	# 	print 'Saved: ' + medical_store.name
	# print 'Highlighted Ones Scraping Finished'

	# Non Highlighted Ones Scraping
	print 'Started Scraping for Non Highlighted Ones'
	for i in range(1,17):
		r = requests.get('http://jaipur.yellowpages.co.in/medical+store?trc=231&page='+str(i))
		root = lxml.html.fromstring(r.content)
		print 'Scraping: ' + r.url

		tmp_list = root.cssselect('div.serpListDn ul li')
		for tmp in tmp_list:
			medical_store = MedicalStore()
			medical_store.name = tmp.cssselect('h2 a')[0].text_content().strip()
			medical_store.link = tmp.cssselect('h2 a')[0].get('href')
			try:
				medical_store.address = tmp.cssselect('p')[0].text_content().strip()
			except IndexError:
				print 'Address Not Found'
			try:
				medical_store.phone = tmp.cssselect('div.phone')[0].text_content().strip()
			except IndexError:
				print 'Phone Not Found'
			medical_store.city = 'Jaipur'
			medical_store.save()
			print 'Saved: ' + medical_store.name
		print 'Finished: ' + r.url
	print 'Finished Scraping Non Highlighted Ones'