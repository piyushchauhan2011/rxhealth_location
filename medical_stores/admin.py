from django.contrib.gis import admin
from models import MedicalStore

# Register your models here.
admin.site.register(MedicalStore, admin.GeoModelAdmin)