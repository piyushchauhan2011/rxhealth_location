# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Doctor.profile_link'
        db.add_column(u'hospitals_doctor', 'profile_link',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Doctor.profile_link'
        db.delete_column(u'hospitals_doctor', 'profile_link')


    models = {
        u'hospitals.award': {
            'Meta': {'object_name': 'Award'},
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'hospitals.contact': {
            'Meta': {'object_name': 'Contact'},
            'appointment': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'billing': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'emergency': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'enquiries': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'hospitals.doctor': {
            'Meta': {'object_name': 'Doctor'},
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'profile_link': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'specialization': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'hospitals.hospital': {
            'Meta': {'object_name': 'Hospital'},
            'about': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locality': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.BigIntegerField', [], {}),
            'pincode': ('django.db.models.fields.IntegerField', [], {'null': 'True'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {'null': 'True'}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        u'hospitals.review': {
            'Meta': {'object_name': 'Review'},
            'facility': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'promptness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'quality_of_care': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'review': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'staff': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'hospitals.service': {
            'Meta': {'object_name': 'Service'},
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'hospitals.speciality': {
            'Meta': {'object_name': 'Speciality'},
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['hospitals']