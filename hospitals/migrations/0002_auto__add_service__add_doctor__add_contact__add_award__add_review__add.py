# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Service'
        db.create_table(u'hospitals_service', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hospital', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospitals.Hospital'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'hospitals', ['Service'])

        # Adding model 'Doctor'
        db.create_table(u'hospitals_doctor', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hospital', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospitals.Hospital'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('specialization', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'hospitals', ['Doctor'])

        # Adding model 'Contact'
        db.create_table(u'hospitals_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hospital', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospitals.Hospital'])),
            ('enquiries', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('appointment', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('emergency', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('billing', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('website', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'hospitals', ['Contact'])

        # Adding model 'Award'
        db.create_table(u'hospitals_award', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hospital', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospitals.Hospital'])),
            ('name', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'hospitals', ['Award'])

        # Adding model 'Review'
        db.create_table(u'hospitals_review', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hospital', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospitals.Hospital'])),
            ('rating', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('quality_of_care', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('staff', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('facility', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('promptness', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('review', self.gf('django.db.models.fields.TextField')(null=True)),
        ))
        db.send_create_signal(u'hospitals', ['Review'])

        # Adding model 'Speciality'
        db.create_table(u'hospitals_speciality', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('hospital', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospitals.Hospital'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'hospitals', ['Speciality'])

        # Adding field 'Hospital.about'
        db.add_column(u'hospitals_hospital', 'about',
                      self.gf('django.db.models.fields.TextField')(null=True),
                      keep_default=False)


        # Changing field 'Hospital.address'
        db.alter_column(u'hospitals_hospital', 'address', self.gf('django.db.models.fields.CharField')(max_length=255))

    def backwards(self, orm):
        # Deleting model 'Service'
        db.delete_table(u'hospitals_service')

        # Deleting model 'Doctor'
        db.delete_table(u'hospitals_doctor')

        # Deleting model 'Contact'
        db.delete_table(u'hospitals_contact')

        # Deleting model 'Award'
        db.delete_table(u'hospitals_award')

        # Deleting model 'Review'
        db.delete_table(u'hospitals_review')

        # Deleting model 'Speciality'
        db.delete_table(u'hospitals_speciality')

        # Deleting field 'Hospital.about'
        db.delete_column(u'hospitals_hospital', 'about')


        # Changing field 'Hospital.address'
        db.alter_column(u'hospitals_hospital', 'address', self.gf('django.db.models.fields.TextField')())

    models = {
        u'hospitals.award': {
            'Meta': {'object_name': 'Award'},
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'null': 'True'})
        },
        u'hospitals.contact': {
            'Meta': {'object_name': 'Contact'},
            'appointment': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'billing': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'emergency': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'enquiries': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'website': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'hospitals.doctor': {
            'Meta': {'object_name': 'Doctor'},
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'specialization': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'hospitals.hospital': {
            'Meta': {'object_name': 'Hospital'},
            'about': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'phone': ('django.db.models.fields.BigIntegerField', [], {}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {'null': 'True'})
        },
        u'hospitals.review': {
            'Meta': {'object_name': 'Review'},
            'facility': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'promptness': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'quality_of_care': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'rating': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'review': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'staff': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        u'hospitals.service': {
            'Meta': {'object_name': 'Service'},
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'hospitals.speciality': {
            'Meta': {'object_name': 'Speciality'},
            'hospital': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospitals.Hospital']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['hospitals']