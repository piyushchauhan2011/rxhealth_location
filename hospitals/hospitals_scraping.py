from models import Hospital,Contact,Speciality,Service,Doctor,Award

import requests,lxml
from lxml import html

def run(verbose=True):
	print 'Started Scraping Sehat.com Hospitals'
	for i in range(9509,19881):
		try:
			r = requests.get('http://www.sehat.com/hospitals-in-hyderabad/care-hospitals-banjara-hills/'+str(i)+'/0/')
			root = lxml.html.fromstring(r.content)

			hospital = Hospital()
			hospital.sehat_url = r.url
			print 'Scraping: ' + r.url
			tmp = root.cssselect("span[itemprop='name']")
			if(tmp):
				hospital.name = tmp[0].text_content().strip()
			tmp = root.cssselect("span[itemprop='description']")
			if(tmp):
				hospital.about = tmp[0].text_content().strip()
			tmp = root.cssselect("span[itemprop='streetAddress']")
			if(tmp):
				hospital.address = tmp[0].text_content().strip()
			tmp = root.cssselect("span[itemprop='addressLocality']")
			if(tmp):
				hospital.locality = tmp[0].text_content().strip()
			tmp = root.cssselect("span[itemprop='addressRegion']")
			if(tmp):
				hospital.state = tmp[0].text_content().strip()
			tmp = root.cssselect("span[itemprop='postalCode']")
			if(tmp):
				hospital.pincode = tmp[0].text_content().strip()
			hospital.save()

			contact = Contact()
			contact.hospital = hospital
			tmp = root.cssselect("span[itemprop='telephone']")
			if(tmp):
				contact.enquiries = tmp[0].text_content().strip()
			tmp = root.cssselect("ul.se_article_info li")
			try:
				contact.appointment = tmp[1].text_content().strip()
			except IndexError:
				print 'Contact Appointment Not Found'
			try:
				contact.emergency = tmp[2].text_content().strip()
			except IndexError:
				print 'Contact Emergency Not Found'
			try:
				contact.billing = tmp[3].text_content().strip()
			except IndexError:
				print 'Contact Billing Not Found'
			try:
				contact.fax = tmp[4].text_content().strip()
			except IndexError:
				print 'Contact Fax Not Found'
			tmp = root.cssselect("ul.se_article_info li a")
			if(tmp):
				contact.website = tmp[0].text_content().strip()
			contact.save()

			tmp = root.cssselect("ul.se_tab_content li")
			for el in tmp:
				speciality = Speciality()
				speciality.hospital = hospital
				speciality.name = el.text_content().strip()
				speciality.save()

			tmp = root.cssselect("div#services ul li")
			for el in tmp:
				service = Service()
				service.hospital = hospital
				service.name = el.text_content().strip()
				service.save()

			tmp = root.cssselect("div#doctors_list tr")
			if(tmp):
				del tmp[0]
				for t in tmp:
					doctor = Doctor()
					doctor.hospital = hospital
					el = t.cssselect("td")
					try:
						doctor.name = el[0].text_content().strip()
					except IndexError:
						print 'Doctor Name Not Found'
					try:
						doctor.specialization = el[1].text_content().strip()
					except IndexError:
						print 'Doctor Specialization Not Found'
					try:
						doctor.profile_link = el[2].text_content().strip()
					except IndexError:
						print 'Doctor ProfileLink Not Found'
					doctor.save()

			tmp = root.cssselect("div#awards ul li")
			for el in tmp:
				award = Award()
				award.hospital = hospital
				award.name = el.text_content().strip()
				award.save()
			print 'Saved Data for url: ' + r.url
		except:
			print 'Redirect Loop May be or Unknown Error'
	print 'Scraping Finished'