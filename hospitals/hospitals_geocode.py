from models import Hospital
from pygeocoder import Geocoder
from django.contrib.gis.geos import Point

geocoder = Geocoder(api_key='AIzaSyDvncyrGON3jeOV1bCmI_DiDZlApRde1d0')

# Done for Rajasthan and Delhi, Punjab, Haryana,  too
def run(verbose=True):
	print 'Geocoding Started for Madhya Pradesh'
	hospitals = Hospital.objects.filter(state='Madhya Pradesh')
	# hospitals = Hospital.objects.filter(locality__contains='Hyderabad')
	for hospital in hospitals:
		print 'Geocoding Hospital: ' + hospital.name
		full_address = hospital.address + ' ' + hospital.locality + ', ' + hospital.state + ', ' + str(hospital.pincode)
		try:
			results = geocoder.geocode(full_address)
			if(results):
				hospital.point = Point(results[0].longitude, results[0].latitude)
			else:
				print 'Unable to Geocode'
		except:
			print 'Some Error'
		hospital.save()
		print 'Saved: ' + hospital.name
	print 'Geocoding Finished'