import json
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.gis.geos import *
from django.contrib.gis.measure import D
from pygeocoder import Geocoder
from hospitals.models import Hospital
from django.core import serializers

# Create your views here.
@csrf_exempt
def hospitals_latlng(request):
	params = request.GET
	radius = float(params['radius'])
	center = json.loads(params['center'])
	#print 'Radius: ' + radius
	#print center['k']
	#print center['A']
	centerX = float(center['A'])
	centerY = float(center['k'])

	#pnt = Point(75.8075002, 26.8373174)
	pnt = Point(centerX, centerY)
	#qs = Hospital.objects.filter(point__distance_lte=(pnt, D(km=2)))
	qs = Hospital.objects.filter(point__distance_lte=(pnt, D(m=radius)))
	some_data = {
    'hello': 'world',
    'thanks': 23
  }
	#pnt = Point(2,1)
	#pnt2 = Point(4,3)
	#temp = [pnt.geojson,pnt2.geojson]
	#temp_data = json.dumps(temp)
	hospital_data = []
	points = []
	qs_data = serializers.serialize("json", qs)
	#print qs_data
	for tmp in qs:
		points.append(tmp.point.geojson)

	hospital_data.append(points)
	hospital_data.append(qs_data)
	data = json.dumps(some_data)
	points_data = json.dumps(points)
	response = HttpResponse(json.dumps(hospital_data), content_type='application/json')
	response["Access-Control-Allow-Origin"] = "*"  
	response["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS"  
	response["Access-Control-Max-Age"] = "1000"  
	response["Access-Control-Allow-Headers"] = "*" 
	return response