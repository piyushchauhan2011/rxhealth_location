from django.contrib.gis.db import models

# Create your models here.
class Hospital(models.Model):
  name = models.CharField(max_length=255)
  address = models.TextField(default='')
  phone = models.BigIntegerField(default=0)
  point = models.PointField(null=True)
  about = models.TextField(null=True)
  locality = models.CharField(max_length=255,null=True)
  state = models.CharField(max_length=255,null=True)
  pincode = models.IntegerField(null=True)
  sehat_url = models.CharField(max_length=255,null=True)

  objects = models.GeoManager()

  def __unicode__(self):
    return self.name

class Contact(models.Model):
	hospital = models.ForeignKey(Hospital)
	enquiries = models.CharField(max_length=255)
	appointment = models.CharField(max_length=255)
	emergency = models.CharField(max_length=255)
	billing = models.CharField(max_length=255)
	fax = models.CharField(max_length=255)
	website = models.CharField(max_length=255)

	def __unicode__(self):
		return self.enquiries

class Speciality(models.Model):
	hospital = models.ForeignKey(Hospital)
	name = models.CharField(max_length=255)

	def __unicode__(self):
		return self.name

class Service(models.Model):
	hospital = models.ForeignKey(Hospital)
	name = models.TextField(default='')

	def __unicode__(self):
		return self.name

class Doctor(models.Model):
	hospital = models.ForeignKey(Hospital)
	name = models.CharField(max_length=255)
	specialization = models.CharField(max_length=255)
	profile_link = models.CharField(max_length=255,null=True)

	def __unicode__(self):
		return self.name

class Award(models.Model):
	hospital = models.ForeignKey(Hospital)
	name = models.TextField(null=True)

	def __unicode__(self):
		return self.name

class Review(models.Model):
	hospital = models.ForeignKey(Hospital)
	rating = models.IntegerField(default=0)
	quality_of_care = models.IntegerField(default=0)
	staff = models.IntegerField(default=0)
	facility = models.IntegerField(default=0)
	promptness = models.IntegerField(default=0)
	review = models.TextField(null=True)

	def __unicode__(self):
		return self.review