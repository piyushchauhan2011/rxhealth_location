from django.contrib.gis import admin
from models import Hospital
from models import Contact,Speciality,Service,Doctor,Award,Review

# Register your models here.
admin.site.register(Hospital, admin.GeoModelAdmin)
admin.site.register(Contact)
admin.site.register(Speciality)
admin.site.register(Service)
admin.site.register(Doctor)
admin.site.register(Award)
admin.site.register(Review)